using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float forceMagnitude = 1f;
    [SerializeField] private float maxVelocity = 10f;
    [SerializeField] private float rotationSpeed = 1f;

    private Camera mainCamera;
    private Rigidbody rb;
    private Vector3 movementDirection;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        rb = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        EnhancedTouchSupport.Enable();
    }

    private void OnDisable()
    {
        EnhancedTouchSupport.Disable();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInput();

        RotateToFaceVelocity();

        KeepPlayerOnScreen();
    }

    void FixedUpdate()
    {
        if (movementDirection == Vector3.zero) return;

        rb.AddForce(forceMagnitude * Time.deltaTime * movementDirection, ForceMode.Force);

        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxVelocity);
    }

    private void ProcessInput()
    {
        if (Touch.activeTouches.Count > 0)
        {
            //Vector2 touchPosition = new Vector2();

            if (Touch.activeTouches.Count > 0)
            {
                Touch touch = Touch.activeTouches[0];

                if (new Rect(1, 1, Screen.width - 1, Screen.height - 1).Contains(touch.screenPosition))
                {
                    Vector3 worldPosition = mainCamera.ScreenToWorldPoint(touch.screenPosition);

                    movementDirection = transform.position - worldPosition;
                    movementDirection.z = 0;
                    movementDirection.Normalize();

                }
            }
            else
            {
                movementDirection = Vector3.zero;
            }
        }
    }

    private void RotateToFaceVelocity()
    {

        if (rb.velocity == Vector3.zero) return;

        Quaternion targetRotation = Quaternion.LookRotation(rb.velocity, Vector3.back);

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }

    private void KeepPlayerOnScreen()
    {
        Vector3 newPosition = transform.position;

        Vector3 viewPortPosition = mainCamera.WorldToViewportPoint(transform.position);

        if (viewPortPosition.x < 0)
        {
            newPosition.x = -newPosition.x - 0.1f;
        }
        else if (viewPortPosition.x > 1)
        {
            newPosition.x = -newPosition.x + 0.1f;
        }
        if (viewPortPosition.y < 0)
        {
            newPosition.y = -newPosition.y - 0.1f;
        }
        if (viewPortPosition.y > 1)
        {
            newPosition.y = -newPosition.y + 0.1f;
        }

        //if (new Rect(1, 1, Screen.width - 1, Screen.height - 1).Contains(transform.position))
        //{ 

        //}

        transform.position = newPosition;
    }
}
