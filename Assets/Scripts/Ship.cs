using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    [SerializeField] private Material boughtMaterial = null; 

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt(Store.NewShipUnlockedKey, 0) == 1)
        {
            GetComponentInChildren<MeshRenderer>().material = boughtMaterial;
        }

    }
}
