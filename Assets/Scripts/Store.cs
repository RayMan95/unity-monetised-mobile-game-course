using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
using UnityEngine.UI;

public class Store : MonoBehaviour
{
    [SerializeField] private GameObject restoreButton;
    [SerializeField] private Text purchaseButtonText;
    [SerializeField] private TMP_Text purchaseButtonTitleText;
    [SerializeField] private TMP_Text purchaseButtonDescriptionText;

    public const string NewShipUnlockedKey = "NewShipUnlocked";

    private const string NewShipId = "com.rr-unity-1.simpledriving.newship";

    private void Awake()
    {
        if (Application.platform != RuntimePlatform.IPhonePlayer)
        {
            restoreButton.SetActive(false);
        } 
    }

    public void OnPurchaseComplete(Product product)
    {
        if(product.definition.id == NewShipId)
        {
            PlayerPrefs.SetInt(NewShipUnlockedKey, 1);
        }

        Debug.Log($"Successfully sold product {product.definition.id} for {product.metadata.localizedPrice} vbucks");
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureDescription purchaseFailureDescription)
    {
        Debug.LogWarning($"Failed to purchase product: {product.definition.id}. Cause: {purchaseFailureDescription.message}");
    }

    public void OnPurchaseProductFetched(Product product)
    {
        ProductMetadata productMetadata = product.metadata;

        purchaseButtonText.text = productMetadata.localizedPrice.ToString();
        purchaseButtonTitleText.text = productMetadata.localizedTitle;
        purchaseButtonDescriptionText.text = productMetadata.localizedDescription;
    }
}
