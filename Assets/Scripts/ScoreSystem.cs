using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private float scoreMultiplier;

    private float score;
    private bool continueScoring = true;

    // Update is called once per frame
    void Update()
    {
        if (continueScoring)
        {
            score += Time.deltaTime * scoreMultiplier;

            scoreText.text = $"Score: {Mathf.FloorToInt(score)}";
        }
        
    }

    public int StopTimer()
    {
        continueScoring = false;

        scoreText.text = string.Empty;

        return Mathf.FloorToInt(score);
    }

    public void StartTimer()
    {
        continueScoring = true;
    }
}
