using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using Unity.Services.Analytics;
#if UNITY_ANDROID
using UnityEngine.Android;
#elif UNITY_IOS
using UnityEngine.iOS;
#endif

public class AdManager : MonoBehaviour, IUnityAdsShowListener, IUnityAdsLoadListener, IUnityAdsInitializationListener
{
    [SerializeField] private ScoreSystem scoreSystem;

    public static AdManager Instance;
    public string environment = "development";

    private GameOverHandler gameOverHandler;


#if UNITY_ANDROID
    private string gameId = 5301600.ToString();
#elif UNITY_IOS
    private string gameId = 5301600.ToString();
#endif

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void ShowAd(GameOverHandler gameOverHandler)
    {
        this.gameOverHandler = gameOverHandler;

        Advertisement.Show("rewardVideo");
    }

    public void OnUnityAdsAdLoaded(string placementId)
    {
        Debug.Log("Ad successfully loaded (and i think is now in progress?)");
    }

    public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message)
    {
        Debug.LogError($"1/2) Unity ads error: {error}");
        Debug.LogError($"2/2) with message: {message}");
    }

    public void OnUnityAdsShowClick(string placementId)
    {
        Debug.Log("User clicked on ad while it was playing");
    }

    public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState)
    {
        switch (showCompletionState)
        {
            case UnityAdsShowCompletionState.SKIPPED:
                Debug.Log("Ad skipped");
                //ContinueGame();
                break;
            case UnityAdsShowCompletionState.COMPLETED:
                Debug.Log("Ad completed");
                break;
            case UnityAdsShowCompletionState.UNKNOWN:
                Debug.LogError("Ad ended with unknown completion state");
                break;
            default:
                Debug.Log("Unexpected ad completion behaviour");
                break;
        }
    }

    public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message)
    {
        Debug.LogError($"1/2) Unity ads error: {error}");
        Debug.LogError($"2/2) with message: {message}");
    }

    public void OnUnityAdsShowStart(string placementId)
    {
        Debug.Log("Unity ad started");
    }

    public void OnInitializationComplete()
    {
        Debug.Log("Unity ad initialization complete");
    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        Debug.LogError($"1/2) Unity ads error: {error}");
        Debug.LogError($"2/2) with message: {message}");
    }

    public void ContinueGame()
    {
        scoreSystem.StartTimer();
    }
}
